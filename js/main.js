$(document).ready(function(){

	window.hash = 'home';

	if(!window.location.hash) {
	$('.home-header').css("opacity", "0").delay(400).fadeTo(800, "1");
	$('.home-subtext').css("opacity", "0").delay(600).fadeTo(800, "1");
	$('#home-bg').css("opacity", "0").delay(1000).fadeTo(1500, "1");
	}

	$('.header-initials').bind('mouseover mouseout', function() {
    $(this).attr({
        src: $(this).attr('data-other-src')
        , 'data-other-src': $(this).attr('src')
    })
});

	// get the value of the bottom of the #main element by adding the offset of that element plus its height, set it as a variable
	var pastMain = $('.home').offset().top + $('.home').height();
	var pastAbout = pastMain + $('.about-section').height();
	var pastDesign = pastAbout + $('.design-section').height();
	var pastCode = pastDesign + $('.code-section').height();

	$('.section-header').mouseover(function() {
		$(this).css('color', '#CCFCCF');
		//console.log($(this).text().toLowerCase());

	}).mouseout(function() {
		if ($(this).text().toLowerCase() != window.hash){
			$(this).css('color', '#CCCCCC');
		}
		else {
			resetHeaderColors();
		}
	})


	var resetHeaderColors = function(){
		$('.header-about').css('color', '#CCCCCC');
		$('.header-design').css('color', '#CCCCCC');
		$('.header-code').css('color', '#CCCCCC');
		$('.header-contact').css('color', '#CCCCCC');
	}

	var onHashChange = function(){
		if(window.location.hash) {
			//Puts hash in variable, and removes the # character
			window.hash = window.location.hash.substring(1);

		    switch (hash) {
		    	case 'about':
		    		resetHeaderColors();
		        	$('.header-about').css('color', '#CCFCCF');
		    		break;
		    	case 'design':
		    		resetHeaderColors();
		        	$('.header-design').css('color', '#CCFCCF');
		    		break;
		    	case 'code':
		    		resetHeaderColors();
		        	$('.header-code').css('color', '#CCFCCF');
		    		break;
		    	case 'contact':
		    		resetHeaderColors();
		        	$('.header-contact').css('color', '#CCFCCF');
		    		break;
			    }
		}
	}

	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});



});
